#!/usr/bin/python
from setuptools import setup

with open("README.md", "r") as handle:
    long_description = handle.read()

setup(
    name="bdflib",
    version="2.1.0",
    description="Library for working with BDF font files.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Timothy Allen",
    author_email="screwtape@froup.com",
    url="https://gitlab.com/Screwtapello/bdflib/",
    packages=["bdflib", "bdflib.test"],
    package_data={"bdflib": ["py.typed"]},
    entry_points={
        "console_scripts": [
            "bdflib-embolden = bdflib.tools:embolden",
            "bdflib-expand= bdflib.tools:expand",
            "bdflib-fill = bdflib.tools:fill",
            "bdflib-merge = bdflib.tools:merge",
            "bdflib-passthrough = bdflib.tools:passthrough",
            "bdflib-validate = bdflib.tools:validate",
            "bdflib-xlfd-validate = bdflib.tools:xlfd_validate",
            "bdflib-xlfd-fix = bdflib.tools:xlfd_fix",
        ]
    },
)
